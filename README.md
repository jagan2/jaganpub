# jagan

## About

This repo is a playground for training GAN networks for generating images. It also contains a script for automatic downloading custom dataset from Flickr website. We used python version 3.6.3 while developing this project.

## Dependecies

Install Python dependencies with:
```
pip3 install -r requirements.txt
```

## Dataset downloading 

You can use an external dataset to train your GAN or fetch images from Flickr website using our script. To get Flickr API key needed to do that go here https://www.flickr.com/services/developer/api/. To download dataset e.g. consisting of 2 classes, 1000 elements each use:

```
cd src
python3 urls_scraper.py \
    --api-key <your-api-key> \
    --out-dir jpg \
    --tags "desert sun, sea coast" \
    --n 1000
```
You can browse the content of dataset by looking into jpg/description.json.

## Dataset preprocessing

Preprocessing step crops images to squares of given size and saved in .npy Numpy format. To preprocess downloaded dataset:
```
python3 preprocess.py \
    --in-dir jpg \
    --out-dir npy
```

## Model training

Train GAN. Architecture is defined in configurable JSONS.
```
python3 train.py \
    --g-json ../etc/generator.json \
    --d-json ../etc/discriminator.json \
    --dataset-dir npy \
    --logs-dir training \
    --d-steps 1 --g-steps 1 \
    --validation-interval 50 \
    --save-interval 100 \
    --learning-rate 0.0002 \
    --batch-size 32 \
    --beta1 0.5
```

## Image generation

Generate images with one of your trained generator models.
```
python3 generate_images.py \
    --model-path training/models/<generator-h5-file> \
    --number 20 \
    --show-only
```

# Tests

To run tests:
```
PYTHONPATH=src python3 -m pytest test/ \
    --disable-pytest-warnings
```
