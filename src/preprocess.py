import argparse
from shutil import copyfile
from itertools import repeat
import logging
from multiprocessing import Pool
import numpy as np
import os
from PIL import Image
import time
from tqdm import tqdm

from DataServer import denormalize


logging.basicConfig(level=logging.INFO)


def image_from_path(img_path):
    return Image.open(img_path)


def image_to_rgb(img):
    return img.convert("RGB")


def image_as_rgb_from_path(img_path):
    return image_to_rgb(image_from_path(img_path))


def crop_image_to_square(img):
    w, h, = img.size
    square_side_len = min(w, h)
    left = (w - square_side_len) // 2
    right = left + square_side_len
    top = (h - square_side_len) // 2
    bottom = top + square_side_len
    return img.crop((left, top, right, bottom))


def resize_square_image(img, target_side_len, mutable_img=False):
    if not mutable_img:
        img = img.copy()
    target_size = (target_side_len, target_side_len)
    img.thumbnail(target_size, Image.ANTIALIAS)
    return img


def image_to_np_array(img):
    raw_data = np.array(img.getdata())
    return raw_data.reshape(img.size[0], img.size[1], 3)


def rgb_to_gray(image):
    r, g, b = image[:,:,0], image[:,:,1], image[:,:,2]
    gray = 0.2989 * r + 0.5870 * g + 0.1140 * b
    gray = np.expand_dims(gray, axis=2)
    return gray


def preprocess_image(img_path, npy_path, square_side_len=None, grayscale=False):
    img = image_as_rgb_from_path(img_path)
    img = crop_image_to_square(img)
    if square_side_len is not None:
        img = resize_square_image(img, square_side_len)
    data = np.array(img)
    if grayscale:
        data = rgb_to_gray(data)
    np.save(npy_path, data)


def np_array_to_image(np_array):
    if np_array.shape[2] == 3:
        return Image.fromarray(np_array, 'RGB')
    return Image.fromarray(np_array[:, :, 0], mode='L')


def normalized_np_array_to_image(np_array):
    return np_array_to_image(denormalize(np_array).astype('uint8'))


def preprocess_image_job(data):
    img_path, npy_path, square_side_len, grayscale  = data
    return preprocess_image(img_path, npy_path, square_side_len, grayscale)


def preprocess_images(in_dir, out_dir, size, max_processes=1, grayscale=False):
    os.makedirs(out_dir, exist_ok=True)
    jobs = []
    for class_dir in next(os.walk(in_dir))[1]:
        target_class_dir = os.path.join(out_dir, class_dir)
        os.makedirs(target_class_dir, exist_ok=True)
        src_class_dir = os.path.join(in_dir, class_dir)
        img_paths = next(os.walk(src_class_dir))[2]
        npy_paths = [i[:i.rfind('.')] + '.npy' for i in img_paths]
        img_full_paths = [os.path.join(src_class_dir, p) for p in img_paths]
        npy_full_paths = [os.path.join(target_class_dir, p) for p in npy_paths]
        jobs += [i for i in zip(img_full_paths, npy_full_paths, repeat(size),
                                repeat(grayscale))]
    logging.info('Started processing {} images with {} processes'
                 .format(len(jobs), max_processes))
    start = time.time()
    pool = Pool(max_processes)
    for _ in tqdm(pool.imap_unordered(preprocess_image_job, jobs),
                  total=len(jobs)):
        pass
    #pool.map(preprocess_image_job, jobs)
    end = time.time()
    logging.info('Finished processing {} images with {} processes in {} '
                 'seconds'.format(len(jobs), max_processes, end - start))


def parse_args():
    parser = argparse.ArgumentParser(description='Crop and resize downloaded '
                                     'images to small squares and save them '
                                     'as *.npy numpy array easily consumable '
                                     'by Python libraries.',
                                     # For newlines in help
                                     formatter_class=
                                     argparse.RawTextHelpFormatter)
    parser.add_argument('--in-dir', type=str, required=True,
                        help="Directory with downloaded images. It is "
                        "assumed that the structure is:\n\n"
                        "<in-dir>/\n"
                        "   <class-1>/\n"
                        "       img1.jpg\n"
                        "       img2.png\n"
                        "   <class-2>/\n"
                        "       img3.jpg\n"
                        "       ...")

    parser.add_argument('--out-dir', type=str, required=True,
                        help="Directory with preprocessed images")
    parser.add_argument('--size', type=int, default=64,
                        help="Length of output square image")
    parser.add_argument('--max-processes', type=int, default=1,
                        help="Number of processes to preprocess on")
    parser.add_argument('--grayscale', action='store_true',
                        help="Add this flag to convert images to grayscale")


    return parser.parse_args()


if __name__ == '__main__':
    # TODO: description.json should contain npy and image size, this is not a
    # must have for now though
    args = parse_args()
    if 64 < args.size:
        logging.warning('Warning: GANs for images bigger than 64x64 are very '
                        'unstable. You are using {0}x{0}.'.format(args.size))
    preprocess_images(args.in_dir, args.out_dir, args.size, args.max_processes,
                      args.grayscale)
