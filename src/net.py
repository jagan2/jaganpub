from collections import OrderedDict
from keras import backend as K
from keras.activations import relu
from keras.models import Sequential, Model, load_model
from keras.layers import Input, Dense, Conv2D, Deconv2D, MaxPooling2D, \
    AveragePooling2D, Reshape, Flatten, LeakyReLU, Concatenate, UpSampling2D, \
    Dropout, BatchNormalization, Activation, RepeatVector
from functools import reduce
import numpy as np


# Use Tenforflow image dimension ordering - [batch, width, height, channels]
K.set_image_dim_ordering('tf')


INPUT = 'input'
OUTPUT = 'output'
INPUT_RECONSTRUCTION = 'input_recostruction'
CONDITIONING = 'conditioning'


class ModelWrapper:
    """
    A wrappper around Keras model that handles input and output as dict.
    """

    def __init__(self, inputs: dict, outputs: dict):
        self._input_dict = OrderedDict(inputs)
        self._output_dict = OrderedDict(outputs)
        self._model = Model(
            inputs=list(self._input_dict.values()),
            outputs=list(self._output_dict.values())
        )

    @staticmethod
    def from_path(path):
        model = load_model(path)
        return ModelWrapper.from_model(model)

    @staticmethod
    def from_model(model):
        inputs = model.inputs
        outputs = model.outputs
        input_dict = OrderedDict({INPUT: inputs[0]})
        if len(inputs) == 2:
            input_dict[CONDITIONING] = inputs[1]
        output_dict = OrderedDict({OUTPUT: outputs[0]})
        if len(outputs) == 2:
            output_dict[INPUT_RECONSTRUCTION] = outputs[1]
        return ModelWrapper(input_dict, output_dict)

    def predict(self, inputs: dict) -> dict:
        assert set(inputs.keys()) == set(self._input_dict)
        outputs = self._model.predict(self._input_dict_to_list(inputs))
        if type(outputs) is not list:
            outputs = [outputs]
        return {k: v for k, v in zip(self._output_dict.keys(), outputs)}

    def train_on_batch(self, inputs: dict, outputs: dict):
        return self._model.train_on_batch(
            self._input_dict_to_list(inputs),
            self._output_dict_to_list(outputs)
        )

    def __call__(self, inputs):
        return self._model([inputs[k] for k in self._input_dict.keys()])

    def compile(self, **kwargs):
        return self._model.compile(**kwargs)

    def set_trainable(self, trainable):
        self._model.trainable = trainable

    def save(self, path):
        self._model.save(path)

    def summary(self):
        return self._model.summary()

    @property
    def input_shape(self):
        input_shapes = self._model.input_shape
        if type(input_shapes) is not list:
            input_shapes = [input_shapes]
        return {k: v for k, v in
                zip(self._input_dict.keys(), input_shapes)}

    def _input_dict_to_list(self, inputs: dict):
        return [inputs[k] for k in self._input_dict.keys()]

    def _output_dict_to_list(self, outputs: dict):
        return [outputs[k] for k in self._output_dict.keys()]


def get_trainable_model(net_dict, conditioning_size=None):
    """
    Builds keras neural network Model consisting of following types of layers:
    - fully connected
    - convolutional
    - pool
    - deconvolutional
    - reshape
    - flatten

    :param net_dict: config dict validated against config.schemas.net.NET_SCHEMA
    :param conditioning_size: amount of classes to use, None if network is not
        conditioned
    :returns: ModelWrapper implementing provided architecture capable of being
        trained and doing forwad pass
    """
    if not 'input_shape' in net_dict:
        raise RuntimeError('No input shape specified')
    if not 'layers' in net_dict:
        raise RuntimeError('No network layers specified')

    input_shape = net_dict['input_shape']
    input_data = Input(input_shape, name='input')
    x = input_data
    conditioning = None
    conditioning_used = False
    last_dense = None
    if conditioning_size is not None:
        conditioning = Input([conditioning_size], name=CONDITIONING)
    for i, l in enumerate(net_dict['layers']):
        if i == len(net_dict['layers']) - 1:
            name = 'output'
        else:
            name = None
        layer_type = l['type']
        if layer_type == 'dense':
            x = Dense(
                units=l['output_size'],
                activation=l['activation'],
                name=name
            )(x)
            last_dense = x
        elif layer_type == 'flatten':
            x = Flatten(name=name)(x)
        elif layer_type == 'reshape':
            x = Reshape(l['shape'], name=name)(x)
        elif layer_type == 'conv':
            x = Conv2D(
                filters=l['filters'],
                strides=(l['strides'], l['strides']),
                kernel_size=l['kernel_size'],
                activation=l['activation'],
                padding='same',
                name=name
            )(x)
        elif layer_type == 'deconv':
            x = Deconv2D(
                filters=l['filters'],
                strides=(l['strides'], l['strides']),
                kernel_size=l['kernel_size'],
                activation=l['activation'],
                padding='same',
                name=name
            )(x)
        elif layer_type == 'pool':
            pool_type = l.get('pool_type') or 'avg'
            if pool_type == 'avg':
                x = AveragePooling2D(
                    pool_size=(l['pool_size'], l['pool_size']),
                    strides=l['strides'],
                    name=name
                )(x)
            elif pool_type == 'max':
                x = MaxPooling2D(
                    pool_size=(l['pool_size'], l['pool_size']),
                    strides=l['strides'],
                    name=name
                )(x)
            else:
                raise RuntimeError('Unknown pool type {}'.format(pool_type))
        elif layer_type == 'upsample':
            x = UpSampling2D(size=l['size'], name=name)(x)
        elif layer_type == 'leakyrelu':
            # This activation is treated as separate layer because in Keras it
            # is not available as 'activation' argument.
            x = LeakyReLU(alpha=0.2, name=name)(x)
        elif layer_type == 'relu':
            # This activation is treated as separate layer because in Keras it
            # is not available as 'activation' argument.
            x = Activation("relu")(x)
        elif layer_type == 'tanh':
            # This activation is treated as separate layer because in Keras it
            # is not available as 'activation' argument.
            x = Activation("tanh")(x)
        elif layer_type == 'dropout':
            # Disclaimer: it is automatically disabled during inference
            x = Dropout(rate=l['rate'], name=name)(x)
        elif layer_type == 'batchnorm':
            momentum = l.get('momentum') or 0.8
            x = BatchNormalization(momentum=momentum, name=name)(x)
        elif layer_type == 'concat_conditioning':
            if conditioning is None:
                raise RuntimeError('Cannot use conditioning if '
                                    'conditioning size was not specified')
            conditioning_used = True
            target_shape = x.shape.as_list()[1:]
            target_shape[-1] = int(conditioning.shape[-1])
            repeat_factor = reduce(lambda a, b: a * b, target_shape[:-1])
            broadcasted_conditioning = \
                    RepeatVector(repeat_factor)(conditioning)

            broadcasted_conditioning = \
                Reshape(target_shape)(broadcasted_conditioning)
            x = Concatenate(axis=len(target_shape))([x, broadcasted_conditioning])
        else:
            raise RuntimeError('Unknown layer type {}'.format(layer_type))

    if conditioning is not None and not conditioning_used:
        raise RuntimeError('Conditioning used but usage not define in model {}'.format(net_dict))

    inputs = {INPUT: input_data}
    outputs = {OUTPUT: x}

    if conditioning_used:
        inputs[CONDITIONING] = conditioning

    if net_dict.get(INPUT_RECONSTRUCTION):
        if last_dense is None:
            raise RuntimeError('Input recostruction supported only if model '
                               'has dense layers. No dense layers found')
        input_dimensions = len(input_shape)
        if input_dimensions != 1:
            raise RuntimeError('Input recostruction supported only for inputs '
                               'of one dimension. {} dimensions given.'
                               .format(len(input_dimensions)))

        input_len = input_shape[0]
        input_reconstruction = Dense(input_len, name='noise')(last_dense)
        outputs[INPUT_RECONSTRUCTION] = input_reconstruction

    model = ModelWrapper(inputs=inputs, outputs=outputs)
    model.summary()

    return model


def get_noise_vector(size, batch_size=1):
    return np.random.normal(0, 1, (batch_size, size))
    # TODO: network ignored noise from vector of given size, possibly values
    # were to small
    #v = np.random.rand(batch_size, size)
    #lengths = np.linalg.norm(v, axis=1)
    #return (v.T / lengths).T
