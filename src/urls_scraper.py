import argparse
from collections import OrderedDict
from concurrent.futures import ThreadPoolExecutor
import json
import logging
import os
import shutil
import requests
import time


logging.basicConfig(level=logging.INFO)


url = "https://api.flickr.com/services/rest/"


def get_response(api_key, tags, tag_mode='all', page=1):
    img_params = {
        "method": "flickr.photos.search",
        "api_key": api_key,
        "format": "json",
        "tags": ','.join(tags),
        "tag_mode": tag_mode,
        "page": page,
        "license" : 4
    }
    r = requests.get(url, params=img_params)
    r_dict = json.loads(r.text[14:-1])
    return r_dict


def get_photo_url(photo_dict):
    farm_id = photo_dict['farm']
    server_id = photo_dict['server']
    photo_id = photo_dict['id']
    secret = photo_dict['secret']
    return "https://farm{}.staticflickr.com/{}/{}_{}.jpg" \
        .format(farm_id, server_id, photo_id, secret)


def get_urls_list(r_dict):
    #TODO: can be parallelized too, loading img urls is long with big dataset
    result = []
    for photo in r_dict['photos']['photo']:
        result.append(get_photo_url(photo))
    return result


def save_urls_list(urls_list, filename):
    with open(filename, 'w') as f:
        for url in urls_list:
            f.write("{}\n".format(url))


def get_photo_urls(api_key, tags, tag_mode, amount, start_page_idx=1):
    page_idx = start_page_idx
    urls = []
    to_download = amount
    while 0 < to_download:
        logging.info('Requesting page {} for tags {}'.format(page_idx, tags))
        r_dict = get_response(api_key, tags, tag_mode, page_idx)
        new_urls = get_urls_list(r_dict)
        if len(new_urls) == 0:
            logging.info('No more images for tags {}'.format(tags))
            break
        if to_download < len(new_urls):
            new_urls = new_urls[:to_download]
        urls += new_urls
        appended = len(new_urls)
        to_download -= appended
        logging.info('Appended urls to {} images, {} left'
                     .format(appended, to_download))
        page_idx += 1
        logging.info('Found {} image urls out of {} requested for tags {}'
                     .format(len(urls), amount, tags))
    return urls


def download_img(url, img_path):
    try:
        r = requests.get(url)
    except Exception as e:
        logging.info("Ommiting image {}, error occured {}".format(url, e))
        return False

    if r.status_code != 200:
        logging.info('Failed to download image {}. Status code {}'
                     .format(url, r.status_code))
        return False
    with open(img_path, 'wb') as f:
        f.write(r.content)
    return True


def download_img_job(data):
    url, img_path, i, total = data
    logging.info('Started downloading image {}/{}: {}'.format(i, total, url))
    return download_img(url, img_path)


def download_imgs(api_key, out_dir, tags, tag_mode, amount=100,
                  max_threads=70, start_page_idx=1):
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)
    img_urls = get_photo_urls(api_key, tags, tag_mode, amount, start_page_idx)
    img_paths = [os.path.join(out_dir, os.path.basename(u)) for u in img_urls]
    amount = len(img_urls)
    jobs = [(u, p, i + 1, amount)
            for u, p, i in zip(img_urls, img_paths, range(amount))]
    max_threads = min(amount, max_threads)
    logging.info('Started downloading {} images for tags {} with {} workers'
                 .format(amount, tags, max_threads))
    start = time.time()
    with ThreadPoolExecutor(max_workers=max_threads) as e:
        results = list(e.map(download_img_job, jobs))
    end = time.time()
    succesfull = len([r for r in results if r])
    logging.info('Finished downloading images for tags {} with {} workers '
                 'in {} seconds. {} successfull out of {}'
                 .format(tags, max_threads, end - start, succesfull, amount))
    return [img for img, success in zip(img_paths, results) if success]


def parse_args():
    parser = argparse.ArgumentParser(description='Download images ordered '
                                     'into classes from Flickr. Each class '
                                     'is identified by a list of tags.')
    parser.add_argument('--out-dir', type=str, required=True,
                        help="Directory with downloaded images")
    parser.add_argument('--tags', type=str, required=True,
                        help="List of tags, e.g. \"sun vacation\". "
                        "For multiple image classes use coma, e.g. "
                        " \"coast see, mountain snow\"")
    parser.add_argument('--tag-mode', type=str, choices=['any', 'all'],
                        default='all',
                        help="Specifies if image should fit any of the tags "
                        "or all of them")
    parser.add_argument('--api-key', type=str, required=True,
                        help="Flickr api key to download images with")
    parser.add_argument('--n', type=int, default=100,
                        help="Number of images to download in each class")
    parser.add_argument('--max-threads', type=int, default=70,
                        help="Threads used for parallel downloading images")
    parser.add_argument('--start-page-index', type=int, default=1,
                        help="First Flickr to download images from")

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_args()
    data = OrderedDict()
    total_images = 0
    for i, tag_str in enumerate(args.tags.split(',')):
        tag_list = tag_str.split()
        class_name = '_'.join(tag_list)
        out_dir = os.path.join(args.out_dir, class_name)
        img_paths = download_imgs(args.api_key, out_dir, tag_list,
                                  args.tag_mode, args.n,  args.max_threads,
                                  args.start_page_index)
        total_images += len(img_paths)
        data[class_name] = {
            'index': i,
            'total_images': len(img_paths),
            'images': img_paths
        }
    with open(os.path.join(args.out_dir, 'description.json'), 'w') as f:
        description = {
            'total_images': total_images,
            'data': data
        }
        json.dump(description, f, indent=4, ensure_ascii=False)
