import argparse
import logging
import math
from matplotlib import pyplot as plt
import numpy as np
import os
from keras.models import load_model

from net import get_noise_vector, ModelWrapper, INPUT, OUTPUT, CONDITIONING
from preprocess import normalized_np_array_to_image


logging.basicConfig(level=logging.INFO)


class ClassGenerationType:
    NONE = 'none'
    SEPARATE = 'separate'
    SMOOTH = 'smooth'

class ImageGenerator:
    def __init__(self, model):
        self.model = model
        model._model.summary()
        input_shape = self.model.input_shape[INPUT]
        if len(input_shape) != 2 or input_shape[0] != None:
            raise ValueError('Invalid input shape {}. '
                             'Expected (None, <noise-len>)'.format(input_shape))
        self.noise_len = input_shape[1]

    def generate(self, number=1, conditioning=None, noise_vector=None):
        images = []
        if noise_vector is None:
            noise_vector = get_noise_vector(self.noise_len, number)
        input_data = {INPUT: noise_vector}
        if conditioning is not None:
            input_data[CONDITIONING] = conditioning
        return [normalized_np_array_to_image(i)
                for i in self.model.predict(input_data)[OUTPUT]]

    def generate_with_linear_noise(self, number):
        noise_start = get_noise_vector(self.noise_len)
        noise_stop = get_noise_vector(self.noise_len)
        noise_vectors = np.concatenate([noise_start * w + noise_stop * (1 - w)
                                  for w in np.arange(0, 1, 1 / number)])
        return self.generate(noise_vector=noise_vectors)

    def generate_for_each_class(self, number):
        image_batches = []
        for class_index in range(self.classes_num):
            conditioning = np.repeat([self._get_conditioning(class_index)],
                                     number, axis=0)
            image_batches.append(self.generate(number, conditioning))
        return image_batches

    def _get_conditioning(self, class_index):
        return np.eye(self.classes_num)[class_index]

    def generate_class_smooth_transition(self, number):
        if (number - 1) % (self.classes_num - 1) != 0:
            raise ValueError('For smooth transition number of images must be '
                             'multiple of (classes - 1) + 1')
        images = []
        noise_vector = get_noise_vector(self.noise_len)
        transition_length = (number - 1) // (self.classes_num - 1)
        weight_step = 1 / transition_length
        class_weights = np.arange(0, 1, weight_step)
        for current_class in range(self.classes_num - 1):
            cur_class_onehot = self._get_conditioning(current_class)
            next_class_onehot = self._get_conditioning(current_class + 1)
            for weight in class_weights:
                cur_class_weight = 1 - weight
                next_class_weight = weight
                conditioning = np.expand_dims(
                    cur_class_onehot * cur_class_weight \
                    + next_class_onehot * next_class_weight, 0)
                print(conditioning)
                images += self.generate(1, conditioning, noise_vector)
        conditioning = np.expand_dims(next_class_onehot, 0)
        images += self.generate(1, conditioning, noise_vector)
        return images

    @property
    def classes_num(self):
        return self.model._input_dict[CONDITIONING].shape.as_list()[1]

    @staticmethod
    def from_path(model_path):
        return ImageGenerator(ModelWrapper.from_path(model_path))


def show_images(images, rows=None, cols=None):
    imgs_num = len(images)
    if rows is None:
        rows = math.ceil(math.sqrt(imgs_num))
    if cols is None:
        cols = math.ceil(imgs_num / rows)
    fig = plt.figure()
    for index, img in enumerate(images):
        fig.add_subplot(rows, cols, index + 1)
        plt.imshow(img, vmin=0, vmax=255)
    plt.show()


def parse_args():
    parser = argparse.ArgumentParser(
        description='Generate images with trained GAN network')

    parser.add_argument('--model-path', required=True, type=str,
                        help='Path containing keras generator model in h5 '
                        'format')
    parser.add_argument('--number', type=int, default=1,
                        help='Amount of images to be generated')
    parser.add_argument('--linear-noise', action='store_true',
                        help="Set to generate with gradual noise change")
    parser.add_argument('--conditioning', default=ClassGenerationType.NONE,
                        choices=[
                            ClassGenerationType.NONE,
                            ClassGenerationType.SEPARATE,
                            ClassGenerationType.SMOOTH
                        ],
                        help='Type of class conditioning to apply')

    return parser.parse_args()


def main():
    args = parse_args()

    image_generator = ImageGenerator.from_path(args.model_path)
    logging.info('Generating {} images'.format(args.number))

    if args.conditioning == ClassGenerationType.NONE:
        if not args.linear_noise:
            images = image_generator.generate(args.number)
        else:
            images = image_generator.generate_with_linear_noise(args.number)
        show_images(images)
    elif args.conditioning == ClassGenerationType.SEPARATE:
        image_batches = image_generator.generate_for_each_class(args.number)
        for image_batch in image_batches:
            show_images(image_batch)
    elif args.conditioning == ClassGenerationType.SMOOTH:
        images = image_generator.generate_class_smooth_transition(args.number)
        show_images(images, cols=args.number)


if __name__ == '__main__':
    main()
