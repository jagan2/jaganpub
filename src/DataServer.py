# This file contains functions responsible for creating input data generator object
# and data normalization/denormalization. To get specific data generator use DataServer class.
# Example:
#   ds = DataServer(1, 'data/datasets/landscapes/', [1, 2])
#   ds_gen = ds.get_data_generator()
#   for i in range(10):
#        print(next(ds_gen))


import json
from glob import glob
import os
import numpy as np

from net import INPUT, CONDITIONING

MAX_PIX_VAL = 255.
HALF_PIX_VAL = MAX_PIX_VAL / 2.


def normalize(x):
    return (x - HALF_PIX_VAL) / HALF_PIX_VAL


def denormalize(x):
    return (x * HALF_PIX_VAL) + HALF_PIX_VAL


def jpg_file_to_npy(jpg_file, data_dir):
    spl = jpg_file.split("/")
    cl = spl[-2]
    file_name = spl[-1]
    basename = os.path.splitext(file_name)[0]
    npy_file = "{}.npy".format(basename)
    return os.path.join(data_dir, cl, npy_file)


def parallel_shuffle(matrices):
    state = np.random.get_state()
    for m in matrices:
        np.random.shuffle(m)
        np.random.set_state(state)


def get_random_conditioning(classes_num, size):
    classes = np.random.randint(classes_num, size=size)
    return np.eye(classes_num)[classes]


def get_class_round_generator(classes_num):
    eye = np.eye(classes_num)
    i = 0
    while True:
        yield eye[i]
        i = (i + 1) % classes_num


def create_generator(data, bs, labels=None):
    for i in range(0, data.shape[0], bs):
        batch = np.array(normalize(data[i:i + bs]))
        # If there are less images left than needed for batch shape
        if batch.shape[0] != bs:
            return None
        if labels is None:
            yield {INPUT: batch}
        else:
            yield {INPUT: batch, CONDITIONING: labels[i:i + bs]}


class DataServer:
    def __init__(self, bs, data_dir, cl_list=None, image_shape=None):
        """
        :param bs:  batch size
        :param epochs: number of epochs
        :param data_dir: path to 'npy' directory
        :param cl_list: list containing images' classes indexes - the same as
            in description.json; if None use all classes
        """
        self.bs = bs
        self.data_dir = data_dir
        self.cl_list = cl_list
        self.epoch_num = 0
        self.image_shape = image_shape
        self.load_data()

    def load_data(self):
        images = []
        labels = []
        occured_classes = []
        class_index = 0
        for class_dir in sorted(glob(os.path.join(self.data_dir, '*'))):
            class_name = os.path.basename(class_dir)
            occured_classes.append(class_name)
            for img in glob(os.path.join(class_dir, '*.npy')):
                image = np.load(img)
                if self.image_shape == None:
                    self.image_shape = image.shape
                if image.shape == self.image_shape:
                    images.append(image)
                    labels.append(class_index)
                else:
                    print("Wrong image size {}: {}".format(image.shape, img))
            class_index += 1

        images_as_one_array = np.array(images)
        labels_as_one_array = np.eye(len(occured_classes))[labels]
        self.images, self.labels = images_as_one_array, labels_as_one_array
        if self.images.shape[0] == 0:
            raise ValueError('The dataset contains no images. Cannot run '
                             'training')

    def get_data_generator(self, with_labels=False):
        self.epoch_num += 1
        parallel_shuffle([self.images, self.labels])
        if with_labels:
            return create_generator(self.images, self.bs, self.labels)
        return create_generator(self.images, self.bs)

    def total_images(self):
        return self.images.shape[0]

    @property
    def cond_size(self):
        return self.labels.shape[1]
