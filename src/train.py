import matplotlib.pyplot as plt

import argparse
from collections import namedtuple, OrderedDict
import json
from keras import backend as K
from keras.callbacks import TensorBoard
from keras.layers import Input
from keras.models import load_model
from keras.optimizers import Adam, RMSprop
import logging
import numpy as np
import os

from net import INPUT, INPUT_RECONSTRUCTION, OUTPUT, CONDITIONING, \
    ModelWrapper, get_trainable_model, get_noise_vector
from DataServer import denormalize, get_random_conditioning, DataServer
from preprocess import normalized_np_array_to_image

# Use Tensorflow image dimension ordering - [batch, width, height, channels]
from log_utils import TensorboardLogger, ImagesLogger

K.set_image_dim_ordering('tf')


logging.basicConfig(level=logging.INFO)


GanTrainingParams = namedtuple(
    "GanTrainingParams",
    ["lr", "beta1", "beta2", "max_gradient", "batch_size", "max_epochs",
     "val_interval", "save_interval", "d_steps", "g_steps", "cond",
     "gen_path", "discr_path", "optimizer"]
)


class JaganTrainer:

    def __init__(self, generator, discriminator, data_server, params,
                 logs_dir):
        self.G = generator
        self.D = discriminator
        self.data_server = data_server
        self.params = params
        self._init_dirs(logs_dir)

    def _init_dirs(self, logs_dir):
        self.logs_dir = logs_dir
        self.img_dir = os.path.join(logs_dir, 'generated_images')
        self.models_dir = os.path.join(logs_dir, 'models')
        self.tensorboard_dir = os.path.join(logs_dir, 'tensorboard')
        for p in [self.logs_dir, self.img_dir, self.models_dir,
                  self.tensorboard_dir]:
            os.makedirs(p, exist_ok=True)

    def train(self):
        self._prepare_models()
        self._prepare_logging_utils()

        total_imgs = self.data_server.total_images()
        total_iterations = 0

        # Run training loop
        iteration_gen = 0
        iteration_disc = 0
        for epoch in range(self.params.max_epochs):
            batch_generator = self.data_server.get_data_generator(
                self.params.cond > 0)
            epoch_finished = False
            logging.info('Started epoch {}'.format(epoch))
            iteration = 0
            images_done = 0

            # Training loop
            while not epoch_finished:
                logging.info('Epoch {}, iteration {}'.format(epoch, iteration))
                logging.info('Images in dataset done {}/{}'
                            .format(images_done, total_imgs))
                iteration += 1
                total_iterations += 1
                # Train discriminator on generated and real data
                for _ in range(self.params.d_steps):
                    d_loss = self._run_discriminator_train_iteration(
                        batch_generator)
                    if d_loss is None:
                        epoch_finished = True
                        break
                    images_done += self.params.batch_size
                    logging.info('\tDiscriminator: loss {}, acc {}'.format(d_loss[0], d_loss[1]))
                    self.discriminator_logger.write_log(d_loss, iteration_disc)
                    iteration_disc += 1
                # Train generator
                for _ in range(self.params.g_steps):
                    g_loss = self._run_generator_train_iteration()
                    iteration_gen += 1
                    logging.info('\tGenerator loss: {}'.format(g_loss))
                    self.generator_logger.write_log([g_loss], iteration_gen)
                # Generating control images
                if total_iterations % self.params.val_interval == 0:
                    self._generate_sample_images(epoch, iteration)
                # Saving intermediate model
                if total_iterations % self.params.save_interval == 0:
                    self._save_models(epoch, iteration)

    def _prepare_models(self):
        self.optimizer = self._prepare_optimizer()
        self._prepare_generator()
        self._prepare_discriminaror()
        self._prepare_merged_model()

    def _prepare_optimizer(self):
        if self.params.optimizer == "adam":
            if self.params.max_gradient is not None:
                return Adam(self.params.lr, self.params.beta1, self.params.beta2,
                            clipvalue=self.params.max_gradient)
            return Adam(self.params.lr, self.params.beta1, self.params.beta2)
        if self.params.optimizer == "rmsprop":
            return RMSprop(lr=self.params.lr)

    def _prepare_generator(self):
        self.noise_size = self.G.input_shape[INPUT][-1]
        self.g_input = OrderedDict()
        self.g_input[INPUT] = Input(shape=[self.noise_size], name=INPUT)

        if self.params.cond:
            self.cond_tensor = Input(shape=[self.params.cond],
                                     name=CONDITIONING)
            self.g_input[CONDITIONING] = self.cond_tensor
        self.generated_img = self.G(self.g_input)

    def _prepare_discriminaror(self):
        self.D.compile(
            loss='binary_crossentropy',
            optimizer=self.optimizer,
            metrics=['accuracy']
        )
        self.D.set_trainable(False)
        self.d_input = OrderedDict()
        self.d_input[INPUT] = self.generated_img

        if self.params.cond:
            self.d_input[CONDITIONING] = self.cond_tensor
        self.is_real = self.D(self.d_input)

    def _prepare_merged_model(self):
        outputs = {OUTPUT: self.is_real}
        self.merged_model = ModelWrapper(self.g_input, outputs)
        self.merged_model.compile(
            loss='binary_crossentropy',
            optimizer=self.optimizer
        )

    def _prepare_logging_utils(self):
        self.current_tensorboard_path = os.path.join(
            self.tensorboard_dir,
            str(len(os.listdir(self.tensorboard_dir)) + 1)
        )
        self.discriminator_logger = TensorboardLogger(
            self.D, os.path.join(self.current_tensorboard_path, 'disc'),
            ['loss_discriminator', 'accuracy_discriminator']
        )
        self.generator_logger = TensorboardLogger(
            self.G, os.path.join(self.current_tensorboard_path, 'gen'),
            ['loss_generator']
        )
        self.img_saver = ImagesLogger(self.logs_dir, self.params)

    def _get_generator_batch(self):
        noise = get_noise_vector(self.noise_size, self.params.batch_size)
        inputs = {INPUT: noise}
        if self.params.cond:
            inputs[CONDITIONING] = get_random_conditioning(
                self.params.cond, self.params.batch_size)
        outputs = {OUTPUT: np.ones((self.params.batch_size, 1))}
        return inputs, outputs

    def _get_generator_validation_batch(self):
        val_batch_size = self.params.cond or self.params.batch_size
        inputs = {INPUT: get_noise_vector(self.noise_size, val_batch_size)}
        if self.params.cond:
            inputs[CONDITIONING] = np.eye(self.params.cond)
        return inputs

    def _get_real_batch(self, img_generator):
        inputs = next(img_generator, None)
        if inputs is None:
            return None
        outputs = {OUTPUT: np.ones((self.params.batch_size, 1))}
        return inputs, outputs

    def _get_fake_batch(self):
        g_inputs, _ = self._get_generator_batch()
        inputs = {INPUT: self.G.predict(g_inputs)[OUTPUT]}
        if self.params.cond:
            inputs[CONDITIONING] = g_inputs[CONDITIONING]
        outputs = {OUTPUT: np.zeros((self.params.batch_size, 1))}
        return inputs, outputs

    def _get_discriminator_batch(self, img_generator):
        real_batch = self._get_real_batch(img_generator)
        if real_batch is None:
            return None
        fake_batch = self._get_fake_batch()
        return real_batch, fake_batch

    def _run_generator_train_iteration(self):
        inputs, outputs = self._get_generator_batch()
        return self.merged_model.train_on_batch(inputs, outputs)

    def _run_discriminator_train_iteration(self, img_generator):
        d_batch = self._get_discriminator_batch(img_generator)
        if d_batch is None:
            return None
        fake_batch, real_batch = d_batch
        inputs, labels = real_batch
        d_loss_2 = self.D.train_on_batch(inputs, labels)
        inputs, labels = fake_batch
        d_loss_1 = self.D.train_on_batch(inputs, labels)
        return [0.5 * (d_loss_1[0] + d_loss_2[0]), \
                0.5 * (d_loss_1[1] + d_loss_2[1])]

    def _generate_sample_images(self, epoch, iteration):
        inputs = self._get_generator_validation_batch()
        imgs = self.G.predict(inputs)[OUTPUT]
        for class_index, img in enumerate(imgs):
            img_path = os.path.join(
                self.img_dir, 'epoch{}.iter{}{}.png'
                .format(epoch, iteration,
                        '.class{}'.format(class_index) \
                        if self.params.cond else ''))
            img_to_save = normalized_np_array_to_image(img)
            logging.info('Saving generated example image {}'
                        .format(img_path))
            img_to_save.save(img_path)

    def _save_models(self, epoch, iteration):
        model_path = 'epoch{}.iter{}.h5'.format(epoch, iteration)
        g_path = 'generator.' + model_path
        d_path = 'discriminator.' + model_path
        g_full_path = os.path.join(self.models_dir, g_path)
        d_full_path = os.path.join(self.models_dir, d_path)

        logging.info('Saving generator model to {}'.format(g_full_path))
        logging.info('Saving discriminator model to {}'.format(d_full_path))

        self.G.save(g_full_path)
        self.D.save(d_full_path)


def train(generator, discriminator, data_server, params, logs_dir):
    t = JaganTrainer(generator, discriminator, data_server, params, logs_dir)
    t.train()


def parse_args():
    parser = argparse.ArgumentParser(
        description='Train a GAN with a provided dataset using provided '
                    'network architectures.')

    # Files and folders
    parser.add_argument('--g-json', type=str,
                        help='JSON with generator architecture')
    parser.add_argument('--d-json', type=str,
                        help='JSON with dicriminator architecture')
    parser.add_argument('--g-trained-model', type=str)
    parser.add_argument('--d-trained-model', type=str)
    parser.add_argument('--dataset-dir', required=True, type=str,
                        help='Folder with npy dataset')
    parser.add_argument('--logs-dir', required=True, type=str,
                        help='Path to dir with training artifacts')

    # Training params
    parser.add_argument('--learning-rate', default=0.00005, type=float)
    parser.add_argument('--beta1', default=0.9, type=float)
    parser.add_argument('--beta2', default=0.999, type=float)
    parser.add_argument('--max-gradient', default=None, type=float)
    parser.add_argument('--batch-size', default=32, type=int)
    parser.add_argument('--max-epochs', default=100, type=float)
    parser.add_argument('--validation-interval', default=25, type=int)
    parser.add_argument('--save-interval', default=500, type=int)
    parser.add_argument('--d-steps', default=1, type=int)
    parser.add_argument('--g-steps', default=1, type=int)
    parser.add_argument('--optimizer', default='rmsprop',  type=str)
    parser.add_argument('--use-conditioning', action='store_true')

    return parser.parse_args()


def main():
    args = parse_args()

    logging.info('Loading dataset from directory \'{}\''.format(args.dataset_dir))
    data_server = DataServer(args.batch_size, args.dataset_dir)
    logging.info('Loaded dataset of {} images'.
                 format(data_server.total_images()))

    conditioning_size = data_server.cond_size if args.use_conditioning else None
    if args.g_trained_model:
        generator = ModelWrapper.from_path(args.g_trained_model)
    elif args.g_json:
        with open(args.g_json) as f:
            logging.info('Building generator')
            g_json = json.load(f)
            generator = get_trainable_model(g_json, conditioning_size)
    else:
        raise RuntimeError('You must provide either generator json or '
                           'pretrained model')
    if args.d_trained_model:
        discriminator = ModelWrapper.from_path(args.d_trained_model)
    elif args.d_json:
        with open(args.d_json) as f:
            logging.info('Building discriminator')
            d_json = json.load(f)
            discriminator = get_trainable_model(d_json, conditioning_size)
    else:
        raise RuntimeError('You must provide either discriminator json or '
                           'pretrained model')
    params = GanTrainingParams(
        args.learning_rate, args.beta1, args.beta2, args.max_gradient,
        args.batch_size, args.max_epochs, args.validation_interval,
        args.save_interval, args.d_steps, args.g_steps, conditioning_size or 0,
        args.g_json, args.d_json, args.optimizer)
    train(generator, discriminator, data_server, params, args.logs_dir)


if __name__ == '__main__':
    main()
