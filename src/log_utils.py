import json
import os
import tensorflow as tf
from keras.callbacks import TensorBoard
from shutil import copyfile

from DataServer import denormalize
from preprocess import np_array_to_image


class TensorboardLogger:
    """
        Generates logs for Tensorboard.
        :param model: discriminator or generator model.
        :param log_path: output path for logs.
        :param names: logged metrics names.

    """

    def __init__(self, model, log_path, names):
        self.model = model
        self.log_path = log_path
        self.names = names
        self.callback = TensorBoard(log_path)
        self.callback.set_model(model)

    def write_log(self, loss, iter_num):
        for name, value in zip(self.names, loss):
            summary = tf.Summary()
            summary_value = summary.value.add()
            summary_value.simple_value = value
            summary_value.tag = name
            self.callback.writer.add_summary(summary, iter_num)
            self.callback.writer.flush()


class ImagesLogger:
    """
        Creates new directory in output path, and write information about
        current run there.
        :param img_path: output dir.
        :param params: run parameters.
    """


    def __init__(self, log_path, params):
        self.info_path = os.path.join(log_path, 'info')
        self.images_path = os.path.join(log_path, 'genereted_images')
        self.discr_path = params.discr_path
        self.gen_path = params.gen_path

        os.makedirs(log_path, exist_ok=True)
        os.makedirs(self.images_path, exist_ok=True)
        os.makedirs(self.info_path, exist_ok=True)

        with open(os.path.join(self.info_path, 'params.json'), 'w+') as f:
            f.write(json.dumps(params._asdict()))

        copyfile(self.discr_path, os.path.join(self.info_path, 'discriminator.json'))
        copyfile(self.gen_path, os.path.join(self.info_path, 'generator.json'))

    def save_img(self, img, img_name):
        denormalized_img = denormalize(img)
        img_to_save = np_array_to_image(denormalized_img.astype('uint8'))
        img_to_save.save(os.path.join(self.images_path, img_name))
