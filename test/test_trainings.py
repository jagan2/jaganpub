import json
import numpy as np
import os
import pytest
from tempfile import gettempdir

from DataServer import DataServer
from net import get_trainable_model
from train import train, GanTrainingParams
from test.common import DATA_DIR


G_JSON = os.path.join(DATA_DIR, 'generator.json')
D_JSON = os.path.join(DATA_DIR, 'discriminator.json')
G_GRAY_JSON = os.path.join(DATA_DIR, 'generator_gray.json')
D_GRAY_JSON = os.path.join(DATA_DIR, 'discriminator_gray.json')


class DataServerMock(DataServer):
    def __init__(self, bs=2, color=False):
        channels = 3 if color else 1
        shape = (10, 10, channels)
        super().__init__(bs, None, None, shape)
        self.images = np.random.rand(bs*2+1, 10, 10, channels)
        self.labels = np.random.rand(bs*2+1, 1)

    def load_data(self):
        pass


def generator(color=False):
    with open(G_JSON if color else G_GRAY_JSON) as f:
        return get_trainable_model(json.load(f))


def discriminator(color=False):
    with open(D_JSON if color else D_GRAY_JSON) as f:
        return get_trainable_model(json.load(f))


def train_params(cond=0):
    return GanTrainingParams(
        0.01, 0.9, 0.9, 0.5, 2, 2, 2, 2, 1, 1, cond, G_JSON, D_JSON
    )


testdata = [
    (True),
    (False)
]

@pytest.mark.parametrize("color", testdata)
def test_training(color):
    train(generator(color), discriminator(color), DataServerMock(color=color),
          train_params(), gettempdir())
